#!/bin/sh

if [ -z "$1" ]; then
    echo "Please provide project name"
    exit 1
fi

st-flash --reset write build/$1.bin 0x8000000
